package org.example.ui;

import org.example.models.character.monsters.IMonster;
import org.example.models.character.monsters.Wolf;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

public class GameMap {
    private final int width;
    private final int height;
    private final Position posHero = new Position(-1, -1);
    private final Map<Position, IMonster> monsters = new HashMap<>();
    private int[][] map;

    public GameMap(int width, int height, int nbMonster) {
        this.width = width;
        this.height = height;
        this.initMap();

        this.generateMonster(nbMonster);
    }

    public Position insertHero() {
        SecureRandom secureRandom = new SecureRandom();

        while (posHero.getX() == -1 && posHero.getY() == -1) {
            int x = secureRandom.nextInt(0, this.width);
            int y = secureRandom.nextInt(0, this.height);

            if (this.map[y][x] != 0) {
                continue;
            }
            posHero.setX(x);
            posHero.setY(y);
        }

        this.set(posHero, 2);

        return posHero;
    }

    public void set(Position position, int type) {
        this.map[position.getY()][position.getX()] = type;
    }

    public int get(Position position) {
        int x = position.getX();
        int y = position.getY();

        return this.map[y][x];
    }

    public IMonster getMonster(Position position) {
        if (!monsters.containsKey(position)) throw new RuntimeException();

        return monsters.get(position);
    }

    private void generateMonster(int nbMonster) {
        int generatedMonster = 0;
        SecureRandom secureRandom = new SecureRandom();

        while (generatedMonster < nbMonster) {
            int x = secureRandom.nextInt(0, this.width);
            int y = secureRandom.nextInt(0, this.height);

            if (this.map[y][x] != 0) {
                continue;
            }
            this.map[y][x] = 1;

            int monsterType = secureRandom.nextInt(1, 2);
            IMonster monster = switch (monsterType) {
                case 1 -> new Wolf();
                default -> throw new RuntimeException();
            };

            monster.setPosition(new Position(x, y));

            monsters.put(monster.getPosition(), monster);

            generatedMonster++;
        }
    }

    private void initMap() {
        this.map = new int[height][width];

        for (int line = 0; line < height; line++) {
            for (int col = 0; col < width; col++) {
                this.map[line][col] = 0;
            }
        }
    }

    private String getSprite(int cellType) {
        return switch (cellType) {
            case 1 -> "M";
            case 2 -> "H";
            default -> " ";
        };
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int line = 0; line < height; line++) {
            builder.append("|");
            for (int col = 0; col < width; col++) {
                switch (this.map[line][col]) {
                    case 1:
                        builder.append("M");
                        break;
                    case 2:
                        builder.append("H");
                        break;
                    default:
                        builder.append(" ");
                        break;
                }
                builder.append("|");
            }
            builder.append("\n");
        }

        return builder.toString();
    }

    public boolean has(Position position) {
        int x = position.getX();
        int y = position.getY();

        if (x < 0 || x >= width) return false;
        return y >= 0 && y < height;
    }

    public void removeMonster(Position position) {
        this.monsters.remove(position);
    }
}
