package org.example.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;

public final class Dice {

    public static int rollDice(Type dice) {
        SecureRandom rnd = new SecureRandom();

        return rnd.nextInt(1, dice.max + 1);
    }

    public static int rollDices(Type dice, int nbRoll, int nbKeep) {
        List<Integer> rolls = new ArrayList<>();

        for(int i = 0; i < nbRoll; i++) {
            rolls.add(rollDice(dice));
        }

        return rolls.stream()
                .mapToInt(i -> i)
                .unordered()
                .limit(nbKeep)
                .sum();
    }
    public static int rollDices(Type dice, int nbRoll) {
        return rollDices(dice, nbRoll, 3);
    }

    public static enum Type {
        d4(4),
        d6(6);

        int max;

        Type(int max) {
            this.max = max;
        }
    }
}
