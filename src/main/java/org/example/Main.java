package org.example;

import org.example.models.character.heroes.Human;
import org.example.models.character.heroes.IHero;
import org.example.models.character.monsters.IMonster;
import org.example.ui.GameMap;
import org.example.ui.Position;

import java.util.Scanner;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        GameMap map = new GameMap(15, 15, 10);

        Position position = map.insertHero();
        IHero hero = new Human();
        hero.setPosition(position);

        System.out.println(map);
        while (hero.getCurrentLp() > 0) {
            Scanner scanner = new Scanner(System.in);

            Position oldPos = hero.getPosition();
            int x = oldPos.getX();
            int y = oldPos.getY();

            String read = scanner.nextLine();
            switch (read) {
                case "z":
                    move(hero, map, new Position(x, y - 1));
                    break;
                case "q":
                    move(hero, map, new Position(x - 1, y));
                    break;
                case "s":
                    move(hero, map, new Position(x, y + 1));
                    break;
                case "d":
                    move(hero, map, new Position(x + 1, y));
                    break;
                default:
                    break;
            }

            System.out.println(map);
        }

    }

    public static boolean canMove(GameMap map, Position position, Consumer<IMonster> fightAction) {
        if (!map.has(position)) return false;
        int element = map.get(position);

        if (element != 0) {
            IMonster monster = map.getMonster(position);

            fightAction.accept(monster);
        }
        return element == 0;
    }

    public static void move(IHero hero, GameMap map, Position newPos) {
        Position oldPos = hero.getPosition();
        if (canMove(map, newPos, (monster) -> fight(hero, monster, map))) {
            hero.setPosition(newPos);
            map.set(newPos, 2);
            map.set(oldPos, 0);
        }
    }

    private static void fight(IHero hero, IMonster monster, GameMap map) {
        while (hero.getCurrentLp() > 0 && monster.getCurrentLp() > 0) {
            hero.hit(monster);
            if (monster.getCurrentLp() > 0) {
                monster.hit(hero);
            }
        }
        if (hero.getCurrentLp() <= 0) {
            map.set(hero.getPosition(), 0);
        } else {
            map.removeMonster(monster.getPosition());
            map.set(monster.getPosition(), 0);
            hero.heal();
        }
    }
}