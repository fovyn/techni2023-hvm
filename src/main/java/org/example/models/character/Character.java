package org.example.models.character;

import lombok.Getter;
import lombok.Setter;
import org.example.models.inventory.Inventory;
import org.example.ui.Position;
import org.example.utils.Dice;

import static com.google.common.base.MoreObjects.toStringHelper;

public abstract class Character implements ICharacter {
    @Getter
    private final int str;
    @Getter
    private final int sta;
    protected Inventory inventory = new Inventory();
    @Getter
    protected int currentLp;

    @Getter
    @Setter
    private Position position;

    public Character(int baseStr, int baseSta) {
        this.str = Dice.rollDices(Dice.Type.d6, 4) + baseStr;
        this.sta = Dice.rollDices(Dice.Type.d6, 4) + baseSta;

//        this.str += this.modifier(this.str);
//        this.sta += this.modifier(this.sta);
        this.currentLp = this.getLp();
    }

    @Override
    public int getLp() {
        return this.sta + modifier(this.sta);
    }


    @Override
    public void hit(ICharacter character) {
        int dmg = Dice.rollDice(Dice.Type.d4) + this.modifier(this.str);
        character.getDommage(dmg);
    }

    @Override
    public void getDommage(int dommage) {
        this.currentLp -= dommage;
        System.out.printf("%s take %d dmg\n", this, dommage);
        if (this.currentLp <= 0) {
            System.out.println("DEAD");
        }
    }

    private int modifier(int stat) {
        if (stat < 5) return -1;
        if (stat < 10) return 0;
        if (stat < 15) return 1;

        return 2;
    }

    @Override
    public String toString() {
        return toStringHelper(this)
                .add("str", str)
                .add("sta", sta)
                .add("currentLp", currentLp)
                .toString();
    }
}
