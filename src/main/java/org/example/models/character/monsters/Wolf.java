package org.example.models.character.monsters;

import org.example.models.inventory.ItemType;
import org.example.utils.Dice;

public class Wolf extends Monster {
    public Wolf() {
        super(0,0);
        inventory.add(ItemType.leather, Dice.rollDice(Dice.Type.d4));
    }
}
