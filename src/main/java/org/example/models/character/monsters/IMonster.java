package org.example.models.character.monsters;

import org.example.models.character.ICharacter;
import org.example.models.inventory.Inventory;

public interface IMonster extends ICharacter {

    Inventory drop();
}
