package org.example.models.character.monsters;

import org.example.models.character.Character;
import org.example.models.inventory.Inventory;

public abstract class Monster extends Character implements IMonster {
    public Monster(int baseStr, int baseSta) {
        super(baseStr, baseSta);
    }

    public Inventory drop() {
        return this.inventory;
    }
}
