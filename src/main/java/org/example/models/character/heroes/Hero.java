package org.example.models.character.heroes;

import org.example.models.character.Character;
import org.example.models.inventory.Inventory;

public abstract class Hero extends Character implements IHero {
    public Hero(int baseStr, int baseSta) {
        super(baseStr, baseSta);
    }

    @Override
    public void loot(Inventory inventory) {
        this.inventory.add(inventory);
    }

    public void heal() {
        this.currentLp = this.getLp();
    }
}
