package org.example.models.character.heroes;

import org.example.models.character.ICharacter;
import org.example.models.inventory.Inventory;

public interface IHero extends ICharacter {
    void loot(Inventory inventory);

    void heal();
}
