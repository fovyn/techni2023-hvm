package org.example.models.character;

import org.example.ui.Position;

public interface ICharacter {

    int getStr();

    int getSta();

    int getCurrentLp();

    int getLp();

    Position getPosition();

    void setPosition(Position position);

    void hit(ICharacter character);

    void getDommage(int dommage);
}
